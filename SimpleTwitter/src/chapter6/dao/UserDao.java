package chapter6.dao;
import static chapter6.utils.CloseableUtil.*;
import static chapter6.utils.DBUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import chapter6.beans.User;
import chapter6.exception.NoRowsUpdatedRuntimeException;
import chapter6.exception.SQLRuntimeException;
import chapter6.utils.CipherUtil;

public class UserDao {

    public void insert(Connection connection, User user) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO users ( ");
            sql.append("    account, ");
            sql.append("    name, ");
            sql.append("    email, ");
            sql.append("    password, ");
            sql.append("    description, ");
            sql.append("    created_date, ");
            sql.append("    updated_date ");
            sql.append(") VALUES ( ");
            sql.append("    ?, ");                                  // account
            sql.append("    ?, ");                                  // name
            sql.append("    ?, ");                                  // email
            sql.append("    ?, ");                                  // password
            sql.append("    ?, ");                                  // description
            sql.append("    CURRENT_TIMESTAMP, ");  // created_date
            sql.append("    CURRENT_TIMESTAMP ");       // updated_date
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, user.getAccount());
            ps.setString(2, user.getName());
            ps.setString(3, user.getEmail());
            ps.setString(4, user.getPassword());
            ps.setString(5, user.getDescription());

            int count = ps.executeUpdate();
            if (count != 1) {
                throw new NoRowsUpdatedRuntimeException();
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } catch (Exception e) {
            throw new IllegalStateException();
        } finally {
            close(ps);
        }
    }

    public User select(Connection connection, String accountOrEmail, String password) {

        PreparedStatement ps = null;
        try {
            String sql = "SELECT * FROM users WHERE (account = ? OR email = ?) AND password = ?";

            ps = connection.prepareStatement(sql);

            ps.setString(1, accountOrEmail);
            ps.setString(2, accountOrEmail);
            ps.setString(3, password);

            ResultSet rs = ps.executeQuery();

            List<User> users = toUsers(rs);
            if (users.isEmpty()) {
                return null;
            } else if (2 <= users.size()) {
                throw new IllegalStateException("ユーザーが重複しています");
            } else {
                return users.get(0);
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    public User select(Connection connection, int id) {

        PreparedStatement ps = null;
        try {
            String sql = "SELECT * FROM users WHERE id = ?";

            ps = connection.prepareStatement(sql);

            ps.setInt(1, id);

            ResultSet rs = ps.executeQuery();

            List<User> users = toUsers(rs);
            if (users.isEmpty()) {
                return null;
            } else if (2 <= users.size()) {
                throw new IllegalStateException("ユーザーが重複しています");
            } else {
                return users.get(0); //usersから要素番号0の要素を取り出す
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    //実践課題その③
    public User select(Connection connection, String account) {

    	  PreparedStatement ps = null;
    	  try {
    	    String sql = "SELECT * FROM users WHERE account = ?";

    	    ps = connection.prepareStatement(sql);
    	    ps.setString(1, account);

    	    ResultSet rs = ps.executeQuery();

    	    List<User> users = toUsers(rs);
    	    if (users.isEmpty()) {
    	      return null;
    	    } else if (2 <= users.size()) {
    	      throw new IllegalStateException("ユーザーが重複しています");
    	    } else {
    	      return users.get(0);
    	    }
    	  } catch (SQLException e) {
    	    throw new SQLRuntimeException(e);
    	  } finally {
    	    close(ps);
    	  }
    	}

    //実践課題その③

    //実践課題その①
    public User selectPassword(Connection connection, int id) {

        PreparedStatement ps = null;
        try {
            String sql = "SELECT * FROM users WHERE id = ?";

            ps = connection.prepareStatement(sql);

            ps.setInt(1, id);

            ResultSet rs = ps.executeQuery();

            List<User> users = toUsers(rs);
            if (users.isEmpty()) {
                return null;
            } else if (2 <= users.size()) {
                throw new IllegalStateException("ユーザーが重複しています");
            } else {
                return users.get(0);
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
    //実践課題その①

    private List<User> toUsers(ResultSet rs) throws SQLException {

        List<User> users = new ArrayList<User>();
        try {
            while (rs.next()) {
                User user = new User();
                user.setId(rs.getInt("id"));
                user.setAccount(rs.getString("account"));
                user.setName(rs.getString("name"));
                user.setEmail(rs.getString("email"));
                user.setPassword(rs.getString("password"));
                user.setDescription(rs.getString("description"));
                user.setCreatedDate(rs.getTimestamp("created_date"));
                user.setUpdatedDate(rs.getTimestamp("updated_date"));

                users.add(user);
            }
            return users;
        } finally {
            close(rs);
        }
    }

    public void update(User user) {

        Connection connection = null;
        try {
            // パスワード暗号化
            String encPassword = CipherUtil.encrypt(user.getPassword());
            user.setPassword(encPassword);

            connection = getConnection();
            new UserDao().update(connection, user);
            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    public void update(Connection connection, User user) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE users SET ");
            sql.append("    account = ?, ");
            sql.append("    name = ?, ");
            sql.append("    email = ?, ");
            sql.append("    password = ?, ");
            sql.append("    description = ?, ");
            sql.append("    updated_date = CURRENT_TIMESTAMP ");
            sql.append("WHERE id = ?");

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, user.getAccount());
            ps.setString(2, user.getName());
            ps.setString(3, user.getEmail());
            ps.setString(4, user.getPassword());
            ps.setString(5, user.getDescription());
            ps.setInt(6, user.getId());

            int count = ps.executeUpdate();
            if (count == 0) {
                throw new NoRowsUpdatedRuntimeException();
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
}